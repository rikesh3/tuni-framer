window.__imported__ = window.__imported__ || {};
window.__imported__["all/layers.json.js"] = [
	{
		"id": 97,
		"name": "save",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/save.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818708"
	},
	{
		"id": 60,
		"name": "home",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/home.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818804"
	},
	{
		"id": 99,
		"name": "recording",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/recording.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818710"
	},
	{
		"id": 68,
		"name": "record",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/record.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818713"
	},
	{
		"id": 64,
		"name": "menu_record",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/menu_record.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818715"
	},
	{
		"id": 74,
		"name": "metronome2",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/metronome2.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1695142782"
	},
	{
		"id": 95,
		"name": "metronome",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/metronome.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818742"
	},
	{
		"id": 62,
		"name": "menu_metronome",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/menu_metronome.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818744"
	},
	{
		"id": 89,
		"name": "perfect_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/perfect_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818746"
	},
	{
		"id": 93,
		"name": "tighten_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/tighten_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818770"
	},
	{
		"id": 91,
		"name": "loosen_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/loosen_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818772"
	},
	{
		"id": 66,
		"name": "pluck_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/pluck_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818774"
	},
	{
		"id": 87,
		"name": "select_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/select_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818776"
	},
	{
		"id": 70,
		"name": "pick_string",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/pick_string.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1695142784"
	},
	{
		"id": 72,
		"name": "menu_tuner",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/menu_tuner.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1009818802"
	}
]