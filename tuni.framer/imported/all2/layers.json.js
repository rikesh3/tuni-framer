window.__imported__ = window.__imported__ || {};
window.__imported__["all2/layers.json.js"] = [
	{
		"id": 45,
		"name": "main",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/main.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "1480263478"
	},
	{
		"id": 47,
		"name": "metronome",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 500,
			"height": 500
		},
		"maskFrame": null,
		"image": {
			"path": "images/metronome.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 500,
				"height": 500
			}
		},
		"imageType": "png",
		"children": [
			
		],
		"modification": "41089474"
	}
]