#PAGE COMPONENT
page =  new PageComponent 
	width: 320
	height: 320
	backgroundColor: "#FFF"
	scrollVertical: false
	borderRadius: 320/2
page.center()
page.borderRadius = page.width/2
page.x += 2.5

# ADDING MAIN UI SCREENS TO PAGE COMPONENT
staffLayers2 = Framer.Importer.load "imported/nall"
staff_keys = Object.keys(staffLayers2)
for i in staff_keys
	staffLayers2[i].y = -180
	staffLayers2[i].opacity = 0

# # Create layers in a for-loop
i = 0
for j in staff_keys
	layer = new Layer 
		name: j
		superLayer: page.content
		width: 320
		height: 320
		image: staffLayers2[j].image
		backgroundColor: "#fff"
		borderRadius: 320/2
		opacity: 1
		x: 320 * i
		i++
		
		
	
# Iterating through the layers that are on the screen
sublayers = page.content.subLayers
# print sublayers[1].name
sublayers[12].name == "pick_string"
for j in page.subLayers
# 	print j.id
	right = new Layer
		superLayer: sublayers[j]
		x: 405
		y: 350
		opacity: 0
	left = new Layer
		superLayer: sublayers[j]
		x: 197
		y: 350
		opacity: 0
	top = new Layer
		superLayer: sublayers[j]
		x: 300
		y: 250
		opacity: 0
	bottom = new Layer
		superLayer: sublayers[j]
		x: 300
		y: 455
		opacity: 0
# 	if sublayers[j.index].name == "home"
# 	print j.id == 2

# print page.currentPage.name
# print (page.currentPage.name == "home")
# print sublayers[0]
right.on Events.Click, ->
	if page.currentPage.name == "recording"
		page.snapToPage(sublayers[0], true, time: 0)
	else if page.currentPage.name == "menu_tuner"
		#page.snapToPreviousPage()
		page.snapToPage(sublayers[9], true, time: 0)
	else if page.currentPage.name == "menu_metronome"
		page.snapToPage(sublayers[12], true, time: 0)
	else if page.currentPage.name == "menu_record"
		page.snapToPage(sublayers[1], true, time: 0)
	else if page.currentPage.name == "perfect_string"
		page.snapToPage(sublayers[3], true, time: 0)
	else if page.currentPage.name == "metronome2"
		page.snapToPage(sublayers[9], true, time: 0)
	else if page.currentPage.name == "save"
		page.snapToPage(sublayers[12], true, time: 0)
	else
		page.snapToNextPage()


# print page.currentPage.index
left.on Events.Click, ->
	if page.currentPage.name == "menu_tuner"
		#page.snapToPreviousPage()
		page.snapToPage(sublayers[1], true, time: 0)
	else if page.currentPage.name == "menu_metronome"
		page.snapToPage(sublayers[2], true, time: 0)
	else if page.currentPage.name == "menu_record"
		page.snapToPage(sublayers[9], true, time: 0)
	else if page.currentPage.name == "home"
		page.snapToPage(sublayers[12], true, time: 0)
	
	
	
top.on Events.Click, ->
	if (page.currentPage.name == "pluck_string" || page.currentPage.name == "select_string" || page.currentPage.name == "loosen_string" || page.currentPage.name == "tighten_string" || page.currentPage.name == "perfect_string")
		page.snapToPage(sublayers[3], true, time: 0)
	else if page.currentPage.name == "pick_string" 
		page.snapToPage(sublayers[2], true, time: 0)
	else if (page.currentPage.name == "metronome" || page.currentPage.name == "metronome2")
		page.snapToPage(sublayers[9], true, time: 0)
	else if (page.currentPage.name == "record" || page.currentPage.name == "recording" || page.currentPage.name == "save")
		page.snapToPage(sublayers[12], true, time: 0)


bottom.on Events.Click, ->
	if page.currentPage.name == "menu_tuner"
		page.snapToPage(sublayers[3], true, time: 0)
	if page.currentPage.name == "menu_metronome"
		page.snapToPage(sublayers[10], true, time: 0)
	if page.currentPage.name == "menu_record"
		page.snapToPage(sublayers[13], true, time: 0)

# # Staging
page.snapToNextPage()
page.currentPage.opacity = 1

# Update pages
page.on "change:currentPage", ->
	page.previousPage.animate 
		properties:
			opacity: 0.3
			scale: 0.8
		time: 0.4
		
	page.currentPage.animate 
		properties:
			opacity: 1
			scale: 1
		time: 0.4


# ADD SKIN ON TOP OF EVERYTHING ELSE
moto360 = new Layer
	width: 600, height: 900
	image: "images/moto_moto360-mask.png"
moto360.scaleX = 0.60
moto360.scaleY = 0.60
moto360.center()

Events.wrap(window).addEventListener "resize", (event) ->
	moto360.center()
	page.center()
	page.x += 2.5


